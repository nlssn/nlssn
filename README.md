## About me
I enjoy tinkering with code. I mostly build websites, but occasionally also games or a some little app.

## Follow me
You can find me:
- Experimenting with code over at [CodePen](https://codepen.io/nlssn)
- Translating Open Source software over at [Crowdin](https://crowdin.com/profile/nlssn)
- Sharpening my skills over at [freeCodeCamp](https://www.freecodecamp.org/nlssn) and [Codewars](https://www.codewars.com/users/nlssn)
